import 'package:flutter/material.dart';

SnackBar snackbarc(String test) {
  final snackBar = SnackBar(
    content: Text(test),
    action: SnackBarAction(
      label: 'Undo',
      onPressed: () {
        // Some code to undo the change.
      },
    ),
  );
  return snackBar;
}
