// ignore_for_file: avoid_print

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pw_validator/flutter_pw_validator.dart';
import 'package:newapp/utils/snackbar.dart';
import 'package:newapp/view/login.dart';

class Signup extends StatefulWidget {
  const Signup({super.key});

  @override
  State<Signup> createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  bool visbility = true;
  bool visbility2 = true;
  bool a = false;
  bool isvalid = false;

  IconData visibilityIcon = Icons.visibility;
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _fnameController = TextEditingController();
  final TextEditingController _lnameController = TextEditingController();
  final TextEditingController _password2Controller = TextEditingController();
  final TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.lightBlueAccent,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Card(
              color: Colors.white,
              shape: const OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  borderSide: BorderSide(color: Colors.transparent)),
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    Container(
                      height: height * 0.03,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.transparent),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(50)),
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: TextFormField(
                            controller: _fnameController,
                            cursorColor: Theme.of(context).primaryColor,
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.person),
                              labelText: 'First name',
                              labelStyle: TextStyle(
                                color: Theme.of(context).primaryColor,
                              ),
                              border: OutlineInputBorder(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(20)),
                                borderSide: BorderSide(
                                    color: Theme.of(context).primaryColor),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: width * 0.03,
                        ),
                        Expanded(
                          child: TextFormField(
                            controller: _lnameController,
                            cursorColor: Theme.of(context).primaryColor,
                            decoration: InputDecoration(
                              prefixIcon: const Icon(Icons.person),
                              labelText: 'Last name',
                              labelStyle: TextStyle(
                                color: Theme.of(context).primaryColor,
                              ),
                              border: OutlineInputBorder(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(20)),
                                borderSide: BorderSide(
                                    color: Theme.of(context).primaryColor),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: height * 0.01,
                    ),
                    Form(
                      autovalidateMode: AutovalidateMode.always,
                      child: TextFormField(
                        controller: _emailController,
                        keyboardType: TextInputType.emailAddress,
                        cursorColor: Theme.of(context).primaryColor,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.email),
                          labelText: 'Email',
                          labelStyle: TextStyle(
                            color: Theme.of(context).primaryColor,
                          ),
                          border: OutlineInputBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(20)),
                            borderSide: BorderSide(
                                color: Theme.of(context).primaryColor),
                          ),
                        ),
                        validator: (value) => EmailValidator.validate(value!)
                            ? null
                            : "Please enter a valid email",
                      ),
                    ),
                    SizedBox(
                      height: height * 0.01,
                    ),
                    TextFormField(
                      controller: _passwordController,
                      onChanged: (value) {
                        setState(() {});
                      },
                      keyboardType: TextInputType.visiblePassword,
                      cursorColor: Theme.of(context).primaryColor,
                      obscureText: visbility,
                      decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.lock),
                        labelText: 'Password',
                        labelStyle: TextStyle(
                          color: Theme.of(context).primaryColor,
                        ),
                        suffixIcon: GestureDetector(
                          onTap: () {
                            setState(() {
                              visbility = !visbility;
                              if (visbility == false) {
                                visibilityIcon = Icons.visibility_off;
                              } else {
                                visibilityIcon = Icons.visibility;
                              }
                            });
                          },
                          child: Icon(visibilityIcon),
                        ),
                        border: OutlineInputBorder(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(20)),
                          borderSide:
                              BorderSide(color: Theme.of(context).primaryColor),
                        ),
                      ),
                    ),
                    if (_passwordController.text != "" && a == false)
                      FlutterPwValidator(
                          controller: _passwordController,
                          minLength: 6,
                          uppercaseCharCount: 1,
                          numericCharCount: 1,
                          width: 400,
                          height: height * 0.12,
                          onSuccess: () {
                            a = true;
                          },
                          onFail: () {
                            a = false;
                          }),
                    SizedBox(
                      height: height * 0.01,
                    ),
                    TextFormField(
                      controller: _password2Controller,
                      keyboardType: TextInputType.visiblePassword,
                      cursorColor: Theme.of(context).primaryColor,
                      obscureText: visbility2,
                      decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.lock),
                        labelText: 'Password',
                        labelStyle: TextStyle(
                          color: Theme.of(context).primaryColor,
                        ),
                        suffixIcon: GestureDetector(
                          onTap: () {
                            setState(() {
                              visbility2 = !visbility2;
                              if (visbility2 == false) {
                                visibilityIcon = Icons.visibility_off;
                              } else {
                                visibilityIcon = Icons.visibility;
                              }
                            });
                          },
                          child: Icon(visibilityIcon),
                        ),
                        border: OutlineInputBorder(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(20)),
                          borderSide:
                              BorderSide(color: Theme.of(context).primaryColor),
                        ),
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () {
                        isvalid =
                            EmailValidator.validate(_emailController.text);
                        if (_passwordController.text == "" ||
                            _fnameController.text == "" ||
                            _lnameController.text == "" ||
                            _emailController.text == "" ||
                            _password2Controller.text == "") {
                          ScaffoldMessenger.of(context).showSnackBar(
                              snackbarc('Please fill all the fields'));
                        } else if (isvalid == false) {
                          ScaffoldMessenger.of(context).showSnackBar(
                              snackbarc('Please enter a valid email'));
                        } else if (a == false) {
                          ScaffoldMessenger.of(context).showSnackBar(snackbarc(
                              'Password does not meet the requierement'));
                        } else if (_passwordController.text !=
                            _password2Controller.text) {
                          ScaffoldMessenger.of(context).showSnackBar(
                              snackbarc('Passwords does not match'));
                        } else {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(snackbarc('All set'));
                        }
                      },
                      child: const Text("Signup"),
                    ),
                    SizedBox(
                      height: height * 0.01,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('Already Signed up ? '),
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: const Text(
                            "Login",
                            style: TextStyle(color: Colors.blue),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
